package net.proventis.amcharts.tests;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.net.URL;
import java.net.URLConnection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.jupiter.api.Test;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.WebClientOptions;
import com.gargoylesoftware.htmlunit.WebConnection;
import com.gargoylesoftware.htmlunit.WebRequest;
import com.gargoylesoftware.htmlunit.WebResponse;
import com.gargoylesoftware.htmlunit.WebResponseData;
import com.gargoylesoftware.htmlunit.WebWindow;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.javascript.host.Window;
import com.gargoylesoftware.htmlunit.util.NameValuePair;
import com.gargoylesoftware.htmlunit.util.WebConnectionWrapper;

public class AmChartExportTest {

	private Log log = LogFactory.getLog(getClass());

	private int screenWidth = 800;
	private int screenHeight = 600;

	private class ResourceLoader extends WebConnectionWrapper {

		public ResourceLoader(WebConnection webConnection) throws IllegalArgumentException {
			super(webConnection);
		}

		private URL getResource(final String path) {
			String p = path;
			while (p.startsWith("/")) {
				p = p.substring(1);
			}
			ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
			URL url = classLoader.getResource(p);
			return url;
		}

		@Override
		public WebResponse getResponse(WebRequest request) throws IOException {
			URL url = request.getUrl();
			if ("http".equals(url.getProtocol()) && "localhost".equals(url.getHost())) {
				String path = url.getPath();
				URL res = getResource(path);
				if (res != null) {
					String contentType = URLConnection.guessContentTypeFromName(path);
					if (contentType == null) {
						contentType = "application/octet-stream";
						if (path.endsWith(".js")) {
							contentType = "application/javascript";
						} else if (path.endsWith(".html")) {
							contentType = "text/html";
						}
					}

					List<NameValuePair> headers = Collections
							.singletonList(new NameValuePair("Content-Type", contentType));
					byte[] bytes = IOUtils.toByteArray(res);
					log.info("load " + path);
					return new WebResponse(new WebResponseData(bytes, 200, "OK", headers), request, 0L);
				}

				return new WebResponse(
						new WebResponseData(new byte[0], 404, url + " not found", Collections.emptyList()), request,
						0L);
			}
			return getWrappedWebConnection().getResponse(request);
		}
	}

	@Test
	void exportChart() throws Exception {
		try (final WebClient webClient = new WebClient(BrowserVersion.BEST_SUPPORTED)) {
			webClient.setWebConnection(new ResourceLoader(webClient.getWebConnection()));
			WebClientOptions options = webClient.getOptions();

			options.setScreenHeight(screenHeight);
			options.setScreenWidth(screenWidth);

			WebWindow ww = webClient.getCurrentWindow();
			ww.setInnerHeight(screenHeight);
			ww.setInnerWidth(screenWidth);
			ww.setOuterHeight(screenHeight);
			ww.setOuterWidth(screenWidth);

			final HtmlPage page = webClient.getPage("http://localhost/test/test-chart-export.html");

			long timeToWait = TimeUnit.SECONDS.toMillis(6L);

			Window window = page.getEnclosingWindow().getScriptableObject();
			for (long endTime = System.currentTimeMillis() + timeToWait; endTime > System.currentTimeMillis();) {
				int jobs = webClient.waitForBackgroundJavaScript(10L);
				int pendingFrames = window.animateAnimationsFrames();
				if (jobs == 0 && pendingFrames == 0) {
					break;
				}
			}

			String svg = page.getBody().getAttribute("data-svg");
			log.info("svg=\n" + svg);

			// export svg created by exporting.getSVG()
			try (Writer w = new FileWriter("target/js-export.svg")) {
				w.write(svg);
			}
			// export dom svg structure
			try (Writer w = new FileWriter("target/dom-export.svg")) {
				w.write(page.getBody().getFirstChild().getFirstChild().asXml());
			}
		}
	}

}
