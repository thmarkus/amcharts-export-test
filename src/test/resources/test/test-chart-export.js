
am4core.ready(function() {
	console.log('am4core.ready');
	});

function exportSVG(chart) {

	function checkReady() {
		if (!chart.isReady()) {
			setTimeout(checkReady, 50);
		} else {
			console.log('start export');

			chart.exporting.getSVG('svg', undefined, false).then(function(svg) {
				// fix invalid url
				svg = svg.replace(/url\("(.*?)"\)/ig, function(_match, p1) {
					return "url('" + p1 + "')";
				});

				document.body.setAttribute('data-svg', svg);
			});

			// this never finishes!
			chart.exporting.getImage('png').then(function(data) { console.log('data', data); });

		}
	}
	checkReady();

}
